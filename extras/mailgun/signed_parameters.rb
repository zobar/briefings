module Mailgun
  class SignedParameters
    attr_reader :params
    delegate :[], :[]=, to: :params

    def initialize(params={})
      @params = params
    end

    def sign!
      self.time      = Time.new
      self.token     = generate_token
      self.signature = calculated_signature
      params
    end

    def valid?
      signature_valid? && timestamp_valid?
    end

    private

    def api_key
      ENV['MAILGUN_API_KEY'] || ''
    end

    def calculated_signature
      OpenSSL::HMAC.hexdigest OpenSSL::Digest::Digest.new('sha256'),
                              api_key, "#{timestamp}#{token}"
    end

    def generate_token
      SecureRandom.urlsafe_base64 37
    end

    def signature
      self[:signature]
    end

    def signature=(value)
      self[:signature] = value
    end

    def time
      Time.at timestamp.to_i unless timestamp.nil?
    end

    def time=(value)
      self.timestamp = value.to_i
    end

    def timestamp
      self[:timestamp]
    end

    def timestamp=(value)
      self[:timestamp] = value.to_s
    end

    def token
      self[:token]
    end

    def token=(value)
      self[:token] = value
    end

    def signature_valid?
      signature == calculated_signature
    end

    def timestamp_valid?
      valid_time_range.cover? time
    end

    def valid_time_delta
      5.minutes
    end

    def valid_time_range
      valid_time_delta.ago .. valid_time_delta.from_now
    end
  end
end
