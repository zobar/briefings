Briefings::Application.routes.draw do
  root 'roots#show'

  namespace :mailgun, defaults: {format: :json} do
    resource :mime, only: :create
  end
end
