class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :title

  protected

  def title
    t :application
  end
end
