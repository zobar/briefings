class Mailgun::BaseController < ApplicationController
  before_filter      :validate_mailgun_signature
  skip_before_action :verify_authenticity_token

  protected

  def mailgun_signature_valid?
    Mailgun::SignedParameters.new(params).valid?
  end

  def validate_mailgun_signature
    unless mailgun_signature_valid?
      render 'invalid_signature', status: :not_acceptable
    end
  end
end
