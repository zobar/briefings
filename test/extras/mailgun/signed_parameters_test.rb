require 'test_helper'

describe Mailgun::SignedParameters do
  let(:api_key)       {ENV['MAILGUN_API_KEY'] || ''}
  let(:old_timestamp) {6.minutes.ago.to_i}
  let(:timestamp)     {4.minutes.ago.to_i}
  let(:token)         {SecureRandom.urlsafe_base64 37}
  let(:wrong_token)   {SecureRandom.urlsafe_base64 37}
  let(:wrong_api_key) {'key-invalid'}

  let(:old_signature) do
    OpenSSL::HMAC.hexdigest OpenSSL::Digest::Digest.new('sha256'),
                            api_key, "#{old_timestamp}#{token}"
  end

  let(:signature) do
    OpenSSL::HMAC.hexdigest OpenSSL::Digest::Digest.new('sha256'),
                            api_key, "#{timestamp}#{token}"
  end

  let(:wrong_signature) do
    OpenSSL::HMAC.hexdigest OpenSSL::Digest::Digest.new('sha256'),
                            wrong_api_key, "#{timestamp}#{token}"
  end

  it 'does not validate if signature is missing' do
    params = {timestamp: timestamp,
              token:     token}
    Mailgun::SignedParameters.new(params).wont_be :valid?
  end

  it 'does not validate if signed by the wrong key' do
    params = {signature: wrong_signature,
              timestamp: timestamp,
              token:     token}
    Mailgun::SignedParameters.new(params).wont_be :valid?
  end

  it 'does not validate if the timestamp is too old' do
    params = {signature: old_signature,
              timestamp: old_timestamp,
              token:     token}
    Mailgun::SignedParameters.new(params).wont_be :valid?
  end

  it 'does not validate if the wrong token was signed' do
    params = {signature: signature,
              timestamp: timestamp,
              token:     wrong_token}
    Mailgun::SignedParameters.new(params).wont_be :valid?
  end

  it 'does not validate if timestamp is missing' do
    params = {signature: signature,
              token:     token}
    Mailgun::SignedParameters.new(params).wont_be :valid?
  end

  it 'validates if the timestamp & signature are valid' do
    params = {signature: signature,
              timestamp: timestamp,
              token:     token}
    Mailgun::SignedParameters.new(params).must_be :valid?
  end

  it 'does not validate if token is missing' do
    params = {signature: signature,
              timestamp: timestamp}
    Mailgun::SignedParameters.new(params).wont_be :valid?
  end

  it 'generates valid signatures' do
    params = {}
    Mailgun::SignedParameters.new(params).sign!
    Mailgun::SignedParameters.new(params).must_be :valid?
  end
end
