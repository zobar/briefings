ENV['RAILS_ENV'] = 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/rails'

class StrictI18nExceptionHandler < I18n::ExceptionHandler
  def call(exception, locale, key, options)
    raise exception.to_exception
  end
end

I18n.exception_handler = StrictI18nExceptionHandler.new
