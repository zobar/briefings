require 'test_helper'

describe Mailgun::MimesController do
  it 'must create mailgun/mime' do
    post :create, Mailgun::SignedParameters.new(format: :json).sign!
    assert_response :success
  end

  it 'must not create mailgun/mime if not signed' do
    post :create
    assert_response :not_acceptable
  end
end
