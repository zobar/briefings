require 'test_helper'

describe RootsController do
  it 'must show root' do
    get :show
    assert_response :success
  end

  it 'must show root.json' do
    get :show, format: :json
    assert_response :success
  end
end
